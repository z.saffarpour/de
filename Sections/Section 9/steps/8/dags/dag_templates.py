from airflow import DAG, macros
from airflow.operators.bash_operator import BashOperator
from datetime import datetime

default_args = {
    "owner": "Airflow",
    "start_date": datetime(2019, 7, 29),
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "email": "youremail@host.com",
    "retries": 1
}


with DAG(dag_id="macro_and_template", schedule_interval="*/10 * * * *", default_args=default_args, catchup=False) as dag:

    dag.doc_md = __doc__

    t1 = BashOperator(
        task_id="my_param_fixed",
        bash_command="echo {{ params.my_param }}",
        params={"my_param": "Hello world"}
    )

    t2 = BashOperator(
        task_id="my_param_macros",
        bash_command="echo Exec Date : {{ execution_date }}{{ params.seperator }} Next DS : {{ next_ds }}{{ params.seperator }} Random Number : {{ macros.random() }}",
        params={"seperator": " -- "}
    )

    t3 = BashOperator(
        task_id="macro_functions",
        bash_command="echo 'execution date : {{ ds }} modified by macros.ds_add to add 5 days : {{ macros.ds_add(ds, 5) }}'"
    )

    templated_command = """
        {% for i in range(5) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, 7)}}"
        echo "{{ params.my_param }}"
        {% endfor %}
        """

    t4 = BashOperator(
        task_id='templated',
        depends_on_past=False,
        bash_command=templated_command,
        params={'my_param': 'Parameter I passed in'},
        dag=dag,
    )

    t4.doc_md = """\
                #### Task Documentation
                You can document your task using the attributes `doc_md` (markdown),
                `doc` (plain text), `doc_rst`, `doc_json`, `doc_yaml` which gets
                rendered in the UI's Task Instance Details page.
                ![img](http://montcs.bloomu.edu/~bobmon/Semesters/2012-01/491/import%20soul.png)
                """

t1 >> t2 >> [t3, t4]

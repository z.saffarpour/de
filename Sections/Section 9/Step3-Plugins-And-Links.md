##### Create Elasticsearch plugin

- copy `steps/3` files and folder to `plugins` and `dags` folders.
- restart the cluster .
- create an `index pattern` for `tweets` and update the `menu item` located at `plugins\elasticsearch_plugin\menu_links\kibana_tweets.py` (href property)
- trigger the `plugin_elasticsearch_dag` : 
  - check the new `Google` and `View Logs in Seperate tabs` buttons in Task instance info.
  - check out the `Quick Acess Menu` 
- steps done to achieve these improvements :  
  - a `plugin info` file added to plugins directory
  - `GoogleLink` and `ViewLogsLink` added to this file.
  - a new folder named `menu_links` created and `Quick Access` category added  to airflow menu using `kibana_tweets`


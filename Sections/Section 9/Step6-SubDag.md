copy `steps/6` dags folder to main `dags` folder

#### Parent DAG

```python
PARENT_DAG_NAME='subdag_dag'
SUBDAG_DAG_NAME='subdag'

with DAG(
	dag_id=PARENT_DAG_NAME,
	schedule_interval='@daily',
	start_date=datetime(2020, 1, 1, 10, 00, 00),
	catchup=False
) as dag:
	start_task = DummyOperator(task_id='start')	
	subdag_task = SubDagOperator(
			subdag=subdag_factory(PARENT_DAG_NAME, SUBDAG_DAG_NAME, dag.start_date, dag.schedule_interval),
			task_id=SUBDAG_DAG_NAME
		)
	end_task = DummyOperator(task_id='end')
	start_task >> subdag_task >> end_task
```



#### Child DAG

```python
def subdag_factory(parent_dag_name, child_dag_name, start_date, schedule_interval):
	subdag = DAG(
		dag_id='{0}.{1}'.format(parent_dag_name, child_dag_name),
		schedule_interval=schedule_interval,
		start_date=start_date,
		catchup=False)
	with subdag:
		dop_list = [DummyOperator(task_id='subdag_task_{0}'.format(i), dag=subdag) for i in range(5)]

	return subdag
```






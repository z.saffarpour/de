from airflow import DAG,utils
from airflow.operators.bash_operator import BashOperator 
from airflow.operators.dummy_operator import DummyOperator
from datetime import datetime, timedelta
from os import path
from datetime import  date

default_args = {
    "owner": "nikamooz",
    "depends_on_past": False,
    'start_date': datetime(2021, 2, 10, 0, 0), 
    "email": ["smbanaei@ut.ac.ir"],
    "retries": 3,
    "retry_delay": timedelta(minutes=1),
}

dag = DAG("Get-Tweets", default_args=default_args, schedule_interval="* * * * *" , catchup=False );

get_tweets = BashOperator(
    task_id='Get-Sahamyab-Tweets',
    bash_command= "/usr/bin/curl -s -H 'User-Agent:Chrome/81.0' https://www.sahamyab.com/guest/twiter/list?v=0.1 | /usr/bin/jq \'.items[] | [.id, .sendTime, .sendTimePersian, .senderName, .senderUsername, .type, .content] | join(\",\") \' > /home/mojtaba/data/stage/step1/$(date +%s).csv" ,
    # bash_command='curl --retry 10 --output {0} -L -H "User-Agent:Chrome/61.0" --compressed "http://members.tsetmc.com/tsev2/excel/MarketWatchPlus.aspx?d=0"'.format( path.join(EXCEL_FILE_PATH, "{0}_{1}.{2}".format(EXCEL_FILE_NAME, date.today().strftime("%Y_%m_%d"),EXCEL_FILE_EXT))),
    dag=dag,
)

task_dummy = DummyOperator(task_id="Dummy-Operator", dag=dag)


get_tweets >> task_dummy
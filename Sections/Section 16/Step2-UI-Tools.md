## Working With Kafka Open Source UI Tools

setup the cluster using :

```bash
$ docker-compose up -d
```

we will have these UI Tools (mainly from landoop projects : https://hub.docker.com/u/landoop)

- **Kafka Connect UI** : http://localhost:8000

- **Kafka Topics UI** : http://localhost:8001

- **Schema Registry** : http://localhost:8002

- **Zookeeper Navigator** : http://localhost:8003

  - connection String  : zookeeper:2181

  

#### Create A Connector Via CURL

in `kafka connect ui ` click `New` button, select `FilePulseSinkConnector` and in `json` tab, paste this JSON : 

```json
{
   "name":"FilePulseCSV",
   "connector.class":"io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
   "fs.listing.directory.path":"/data/musics/",
   "fs.scan.interval.ms":"10000",
   "fs.scan.filters":"io.streamthoughts.kafka.connect.filepulse.scanner.local.filter.RegexFileListFilter",
   "file.filter.regex.pattern":".*\\.csv$",
   "task.reader.class":"io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
   "offset.strategy":"name",
   "skip.headers":"1",
   "topic":"musics-filepulse-csv",
   "fs.listing.class":"io.streamthoughts.kafka.connect.filepulse.fs.LocalFSDirectoryListing",
   "internal.kafka.reporter.bootstrap.servers":"kafka:29092",
   "tasks.file.status.storage.bootstrap.servers":"kafka:29092",
   "internal.kafka.reporter.topic":"connect-file-pulse-status",
   "fs.cleanup.policy.class":"io.streamthoughts.kafka.connect.filepulse.fs.clean.LogCleanupPolicy",
   "tasks.max":1,
   "filters":"ParseLine",
   "filters.ParseLine.extractColumnName":"headers",
   "filters.ParseLine.trimColumn":"true",
   "filters.ParseLine.separator":";",
   "filters.ParseLine.type":"io.streamthoughts.kafka.connect.filepulse.filter.DelimitedRowFilter"
}
```

create data folder :

```bash
$ docker exec -it connect mkdir -p /data/musics
```

copy music-dataset to destination folder :

```bash
$ docker cp ../Datasets/musics-dataset.csv  connect://data/musics/musics-dataset-00.csv
```

***Now work around with the UI Provided .***

---

##### New Connector :  The Complete Sample

you can create new connector for our last sample : 

```json
{
  "name" : "FilePulseCSV20",
  "connector.class": "io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
  "skip.headers": "1",
  "tasks.max": "1",
  "filters.ParseLine.extractColumnName": "headers",
  "fs.scan.interval.ms": "10000",
  "offset.strategy": "name",
  "internal.kafka.reporter.topic": "connect-file-pulse-status20",
  "filters.ReleaseToInt.overwrite": "true",
  "fs.scan.filters": "io.streamthoughts.kafka.connect.filepulse.scanner.local.filter.RegexFileListFilter",
  "filters.ReleaseToInt.value": "{{ converts($value.release, 'INTEGER') }}",
  "internal.kafka.reporter.bootstrap.servers": "kafka:29092",
  "filters.KeepACDC.invert": "false",
  "fs.listing.class": "io.streamthoughts.kafka.connect.filepulse.fs.LocalFSDirectoryListing",
  "task.reader.class": "io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
  "filters.ReleaseToInt.type": "io.streamthoughts.kafka.connect.filepulse.filter.AppendFilter",
  "filters.KeepACDC.if": "{{ equals($value.artist, 'AC/DC') }}",
  "filters.ParseLine.type": "io.streamthoughts.kafka.connect.filepulse.filter.DelimitedRowFilter",
  "fs.listing.directory.path": "/data/musics/",
  "filters": "ParseLine,KeepACDC,ReleaseToInt",
  "tasks.file.status.storage.bootstrap.servers": "kafka:29092",
  "fs.cleanup.policy.class": "io.streamthoughts.kafka.connect.filepulse.fs.clean.LogCleanupPolicy",
  "topic": "musics-filepulse-csv20",
  "filters.ReleaseToInt.field": "$value.release",
  "filters.ParseLine.separator": ";",
  "filters.ParseLine.trimColumn": "true",
  "filters.KeepACDC.type": "io.streamthoughts.kafka.connect.filepulse.filter.DropFilter",
  "file.filter.regex.pattern": ".*\\.csv$"
}
```

##### Confluent Connector Hub

Check out [Confluent Connector Hub](https://www.confluent.io/hub/) and browse over 90 free Source/Sink Connector


### HW-1 - KSQLDB Easy Samples

- [ksqlDB Tutorial: How to count a stream of events using ksqlDB (confluent.io)](https://kafka-tutorials.confluent.io/create-stateful-aggregation-count/ksql.html)

- [ksqlDB Tutorial: How to sum a stream of events using ksqlDB (confluent.io)](https://kafka-tutorials.confluent.io/create-stateful-aggregation-sum/ksql.html)

- [Clickstream Data Analysis Pipeline Using ksqlDB | Confluent Documentation](https://docs.confluent.io/platform/current/tutorials/examples/clickstream/docs/index.html)

  ![](https://docs.confluent.io/platform/current/_images/clickstream_demo_flow.png)

  - Grafana
  - Confluent Control Center
  - Elasticsearch
  
    

### HW-2 - A Full Python Streaming Sample with Kafka

[enricomarchesin/kafka-data-streaming-example: My project submission for Udacity Data Streaming Nanodegree (github.com)](https://github.com/enricomarchesin/kafka-data-streaming-example)

- python consumer/producer

- Kafka Connect

- KSQL

  ![](https://github.com/enricomarchesin/kafka-data-streaming-example/raw/master/images/diagram.png)



### HW3 - KSQL Workshop

[aweagel/ksql_workshop: Let's Learn ksqlDB (github.com)](https://github.com/aweagel/ksql_workshop)

- KSQLDB
- Postgres
- Elasticsearch

### HW4 - Building A Streaming Fraud Detection System With Kafka And Python

[Building A Streaming Fraud Detection System With Kafka And Python (florimond.dev)](https://florimond.dev/en/posts/2018/09/building-a-streaming-fraud-detection-system-with-kafka-and-python/)

![](https://florimond.dev/static/img/fraud-diagram.png)



### HW5 - Kafka Connect JDBC Sink deep-dive

[Kafka Connect JDBC Sink deep-dive: Working with Primary Keys (rmoff.net)](https://rmoff.net/2021/03/12/kafka-connect-jdbc-sink-deep-dive-working-with-primary-keys/)

- Kafka Connect

- KSQL

- MySQL/Postgres

  

### HW6 - How to build a real-time analytics platform using Kafka, ksqlDB and ClickHouse ?

Apache Kafka + ksqlDB + ClickHouse + Superset = Blazing Fast Analytics Platform

![](Step3-KSQLDB\img\1_mU0ISilHOC4CmmGkatDu5A.png)






## Working With Kafka Connect

### Reference

[Streaming data into Kafka S01/E01 — Loading CSV file | by Florian Hussonnois | StreamThoughts | Medium](https://medium.com/streamthoughts/streaming-data-into-kafka-s01-e01-loading-csv-file-8ea053b232cb)

### Kafka Connect File Pulse connector



![Connect File Pulse Logo](https://www.confluent.io/hub/static/1bb080066c5fafb053b4b0bdf44f79e1/58e9c/logo.png)

The [Kafka Connect File Pulse connector](https://github.com/streamthoughts/kafka-connect-file-pulse) makes it easy to parse, transform, and stream data file into Kafka. It supports several formats of files, but we will focus on CSV. [Connect File Pulse](https://github.com/streamthoughts/kafka-connect-file-pulse) is a multi-purpose Kafka Connect plugin for ingesting and transforming files that was release in open-source a few months ago.

## FilePluse Key Features

Connect File Pulse provides the capability to periodically scan an **input local directory** (recursively or not) for new files and read and transform data as strongly typed records. Records are sent into Apache Kafka ***as new files are added into the input directory or data are added to an existing file***.

As of this writing, Connect File Pulse provides the following readers :

- **RowFileInputReader:** allows reading a file line by line and creates one record per row. It can be used to read, for example, delimited text files (i.e. CSV, TSV) or application log files (e.g. log4j).
- **BytesArrayInputReader:** can be used to create a single byte array record from a source file.
- **AvroFileInputReader:** allows reading input Avro container files.
- **XMLFileInputReader:** allows reading simple XML files. This reader will infer record schema from the XML DOM.

## FilePulse Filters

Connect File Pulse allows you to configure complex filtering chains for parsing, transforming and enriching data through the use of [Filters](https://streamthoughts.github.io/kafka-connect-file-pulse/docs/developer-guide/filters-chain-definition/).

Here are some examples of the provided built-in Filters :

- **AppendFilter:** Appends one or more values to an existing or non-existing array field.
- **DelimitedRowFilter:** Parses a message field’s value containing columns delimited by a separator into a struct.
- **GrokFilter:** Parses an unstructured message field’s value to a struct by combining Grok patterns (similar to Logstash).
- **GroupRowFilter:** Regroups multiple following messages into a single message by composing a grouping key.
- **JSONFilter:** Unmarshallings a JSON message field’s value to a complex struct.

### Setup The Cluster

go into `Step1-FilePulse`  folder (in command prompt) and start the provided kafka cluster : 

```bash
$ docker-compose up -d
```

Our cluster consists of these services:

- **zookeeper**
- **Kafka**
- **Kafka Connect (FilePulse jar file already included)**
- **Schema Registry**

#### WSL

Because we will use `jq` and `grep` frequently, we suggest you to use WSL (on `WINDPWS`)

```bash
$ wsl
```

#### Check Out The Connector Plugin Installed 

use one of these command and look for `FilePulse` Class.

```bash
$ curl -s localhost:8083/connector-plugins | jq
$ curl -s localhost:8083/connector-plugins|jq '.[].class'
$ curl -s localhost:8083/connector-plugins|jq '.[].class'|egrep FilePulse
```

#### Let's Create a Connector 

```bash
$ curl \
 -i  -X PUT -H "Accept:application/json"     \
-H  "Content-Type:application/json" http://localhost:8083/connectors/music-csv-filepulse/config \
-d '
{
    "connector.class": "io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
    "fs.listing.directory.path": "/data/musics/",
    "fs.scan.interval.ms": "10000",
    "fs.scan.filters": "io.streamthoughts.kafka.connect.filepulse.scanner.local.filter.RegexFileListFilter",
    "file.filter.regex.pattern": ".*\\.csv$",
    "task.reader.class": "io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
    "offset.strategy": "name",
    "skip.headers": "1",
    "topic": "musics-filepulse-csv",
    "internal.kafka.reporter.bootstrap.servers": "kafka:29092",
    "tasks.file.status.storage.bootstrap.servers": "kafka:29092",
    "internal.kafka.reporter.topic": "connect-file-pulse-status",
    "fs.cleanup.policy.class": "io.streamthoughts.kafka.connect.filepulse.fs.clean.LogCleanupPolicy",
    "fs.listing.class": "io.streamthoughts.kafka.connect.filepulse.fs.LocalFSDirectoryListing",
    "tasks.max": 1
}
'
```

we would see the followings : (201 Return Code)

```bash
HTTP/1.1 201 Created
Date: Fri, 09 Jul 2021 16:41:12 GMT
Location: http://localhost:8083/connectors/music-csv-filepulse
Content-Type: application/json
Content-Length: 800
Server: Jetty(9.4.24.v20191120)

{"name":"music-csv-filepulse","config": 
.....
```

check out everything is OK :

```bash
$  curl -s localhost:8083/connectors 
["music-csv-filepulse"]
$  curl -s localhost:8083/connectors/music-csv-filepulse | jq
{
  "name": "music-csv-filepulse",
  "config": {
    "connector.class": "io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
    "task.reader.class": "io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
    "skip.headers": "1",
    ....
$  curl -s localhost:8083/connectors/music-csv-filepulse/status | jq    
{
  "name": "music-csv-filepulse",
  "connector": {
    "state": "RUNNING",
    "worker_id": "connect:8083"
  },
  "tasks": [],
  "type": "source"
}

```

create data folder :

```bash
$ docker exec -it connect mkdir -p /data/musics
```

copy music-dataset to destination folder :

```bash
$ docker cp ../Datasets/musics-dataset.csv  connect://data/musics/musics-dataset-00.csv
```

now the data must be appeared :

```bash
$ kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1
{"topic":"musics-filepulse-csv","partition":0,"offset":108,"tstype":"create","ts":1626014652810,"headers":["connect.file.name","musics-dataset-00.csv","connect.file.uri","file:/data/musics/musics-dataset-00.csv","connect.file.contentLength","6588","connect.file.lastModified","1625853269000","connect.file.system.inode","544687","connect.file.system.hostname","connect","connect.task.hostname","connect","connect.file.hash.digest","1466679696","connect.file.hash.algorithm","CRC32"],"key":null,"payload":"\u0000\u0000\u0000\u0000\u0001\u0002\u0002VZoo Station;Achtung Baby;04:36;1991;U2;Rock\u0002\u0002\u0002Ptitle;album;duration;release;artist;type\u0000"}

$ kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1 -s key=s -s value=avro -r http://localhost:8081
% ERROR: This build of kafkacat lacks Avro/Schema-Registry support

```

#### Use Docker to run Kafkacat with Avro Support

```bash
$ docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1
{"topic":"musics-filepulse-csv","partition":0,"offset":108,"tstype":"create","ts":1626014652810,"broker":1,"headers":["connect.file.name","musics-dataset-00.csv","connect.file.uri","file:/data/musics/musics-dataset-00.csv","connect.file.contentLength","6588","connect.file.lastModified","1625853269000","connect.file.system.inode","544687","connect.file.system.hostname","connect","connect.task.hostname","connect","connect.file.hash.digest","1466679696","connect.file.hash.algorithm","CRC32"],"key":null,"payload":"\u0000\u0000\u0000\u0000\u0001\u0002\u0002VZoo Station;Achtung Baby;04:36;1991;U2;Rock\u0002\u0002\u0002Ptitle;album;duration;release;artist;type\u0000"}

$  docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1 -s key=s -s value=avro -r http://schema-registry:8081

{"topic":"musics-filepulse-csv","partition":0,"offset":108,"tstype":"create","ts":1626014652810,"broker":1,"headers":["connect.file.name","musics-dataset-00.csv","connect.file.uri","file:/data/musics/musics-dataset-00.csv","connect.file.contentLength","6588","connect.file.lastModified","1625853269000","connect.file.system.inode","544687","connect.file.system.hostname","connect","connect.task.hostname","connect","connect.file.hash.digest","1466679696","connect.file.hash.algorithm","CRC32"],"key":null,"payload":{"ConnectDefault": {"message": {"string": "Zoo Station;Achtung Baby;04:36;1991;U2;Rock"}, "headers": {"array": [{"string": "title;album;duration;release;artist;type"}]}}}}


$ docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1 -s key=s -s value=avro -r http://schema-registry:8081 | jq -c .payload.ConnectDefault.message
{"string":"Zoo Station;Achtung Baby;04:36;1991;U2;Rock"}

```

#### Headers Info

The `FilePulse` connector will add headers to each record containing metadata about the source file.

```bash
$ docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv -J -q -o-1 -s key=s -s value=avro -r http://schema-registry:8081 | jq -c .headers

[
"connect.file.name","musics-dataset-00.csv",
"connect.file.uri","file:/data/musics/musics-dataset-00.csv",
"connect.file.contentLength", "6588",
"connect.file.lastModified","1625853269000",
"connect.file.system.inode","544687",
"connect.file.system.hostname","connect",
"connect.task.hostname","connect",
"connect.file.hash.digest","1466679696",
"connect.file.hash.algorithm","CRC32"
]

```

#### Parsing data

The File Pulse connector allows us to define complex pipelines to parse, transform, and enrich data through the use of processing [Filters](https://streamthoughts.github.io/kafka-connect-file-pulse/docs/developer-guide/filters-chain-definition/).

You can use the built-in [DelimitedRowFilter](https://streamthoughts.github.io/kafka-connect-file-pulse/docs/developer-guide/filters/#delimitedrowfilter) to parse each line. Also, because the first line of the CSV file is a header you can set the property `extractColumnName` to name the record's fields based on the `headers` field.

Let’s create a new connector with this new configuration :

```bash
$ curl \
    -i -X PUT -H "Accept:application/json" \
    -H  "Content-Type:application/json" http://localhost:8083/connectors/music-csv-filepulse2/config \
    -d '{
        "connector.class":"io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
        "fs.listing.directory.path":"/data/musics/",
        "fs.scan.interval.ms":"10000",
     "fs.scan.filters":"io.streamthoughts.kafka.connect.filepulse.scanner.local.filter.RegexFileListFilter",
        "file.filter.regex.pattern":".*\\.csv$",
        "task.reader.class": "io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
        "offset.strategy":"name",
        "skip.headers": "1",
        "topic":"musics-filepulse-csv2",
		"fs.listing.class":"io.streamthoughts.kafka.connect.filepulse.fs.LocalFSDirectoryListing",
        "internal.kafka.reporter.bootstrap.servers": "kafka:29092",
        "tasks.file.status.storage.bootstrap.servers": "kafka:29092",
        "internal.kafka.reporter.topic":"connect-file-pulse-status2",
        "fs.cleanup.policy.class": "io.streamthoughts.kafka.connect.filepulse.fs.clean.LogCleanupPolicy",
        "tasks.max": 1,
        "filters":"ParseLine",
        "filters.ParseLine.extractColumnName": "headers",
        "filters.ParseLine.trimColumn": "true",
        "filters.ParseLine.separator": ";",
        "filters.ParseLine.type": "io.streamthoughts.kafka.connect.filepulse.filter.DelimitedRowFilter"
    }'
```

check out that this new connector is created :

```bash
$ curl -s localhost:8083/connectors
["music-csv-filepulse2"]

$ docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv2 -J -q -o-1 -s key=s -s value=avro -r http://schema-registry:8081 | jq -c .payload.ConnectDefault
{"title":{"string":"Zoo Station"},"album":{"string":"Achtung Baby"},"duration":{"string":"04:36"},"release":{"string":"1991"},"artist":{"string":"U2"},"type":{"string":"Rock"}}

```

#### Change Field Type / Drop(or Keep) Records

Let's change `release` type from `string` into `integer` and keep only the `AC/DC` band .

For doing this, you can use the `AppendFilter` with the Simple connect Expression

```bash
$  curl \
    -i -X PUT -H "Accept:application/json" \
    -H  "Content-Type:application/json" http://localhost:8083/connectors/music-csv-filepulse20/config \
    -d '{
        "connector.class":"io.streamthoughts.kafka.connect.filepulse.source.FilePulseSourceConnector",
        "fs.listing.directory.path":"/data/musics/",
        "fs.scan.interval.ms":"10000",
     "fs.scan.filters":"io.streamthoughts.kafka.connect.filepulse.scanner.local.filter.RegexFileListFilter",
        "file.filter.regex.pattern":".*\\.csv$",
        "task.reader.class": "io.streamthoughts.kafka.connect.filepulse.reader.RowFileInputReader",
        "offset.strategy":"name",
        "skip.headers": "1",
        "topic":"musics-filepulse-csv20",
		"fs.listing.class":"io.streamthoughts.kafka.connect.filepulse.fs.LocalFSDirectoryListing",
        "internal.kafka.reporter.bootstrap.servers": "kafka:29092",
        "tasks.file.status.storage.bootstrap.servers": "kafka:29092",
        "internal.kafka.reporter.topic":"connect-file-pulse-status20",
        "fs.cleanup.policy.class": "io.streamthoughts.kafka.connect.filepulse.fs.clean.LogCleanupPolicy",
        "tasks.max": 1,
        "filters.ParseLine.extractColumnName": "headers",
        "filters.ParseLine.trimColumn": "true",
        "filters.ParseLine.separator": ";",
        "filters.ParseLine.type": "io.streamthoughts.kafka.connect.filepulse.filter.DelimitedRowFilter",

		"filters":"ParseLine,KeepACDC,ReleaseToInt",
        
        "filters.KeepACDC.type":"io.streamthoughts.kafka.connect.filepulse.filter.DropFilter",
        "filters.KeepACDC.if":"{{ equals($value.artist, '\''AC/DC'\'') }}",
        "filters.KeepACDC.invert":"true",
        "filters.ReleaseToInt.type": "io.streamthoughts.kafka.connect.filepulse.filter.AppendFilter",
        "filters.ReleaseToInt.field": "$value.release",
        "filters.ReleaseToInt.value": "{{ converts($value.release, '\''INTEGER'\'') }}",
        "filters.ReleaseToInt.overwrite": "true"
        
        
    }'
```

check the final result :

```bash
$  docker run --rm --tty --interactive --network=kafka-network edenhill/kafkacat:1.6.0 kafkacat -b kafka:29092 -C -t musics-filepulse-csv20 -J -q -o-3 -s key=s -s value=avro -r http://schema-registry:8081 | jq -c .payload.ConnectDefault
{"title":{"string":"Shoot to Thrill"},"album":{"string":"Back in Black"},"duration":{"string":"5:17"},"artist":{"string":"AC/DC"},"type":{"string":"Hard Rocks"},"release":{"int":1980}}
                                                  {"title":{"string":"What Do You Do for Money Honey"},"album":{"string":"Back in Black"},"duration":{"string":"3:37"},"artist":{"string":"AC/DC"},"type":{"string":"Hard Rocks"},"release":{"int":1980}}
                                                                                                                   {"title":{"string":"You Shook Me All Night Long"},"album":{"string":"Back in Black"},"duration":{"string":"3:32"},"artist":{"string":"AC/DC"},"type":{"string":"Hard Rocks"},"release":{"int":1980}}
```

#### Working With Schema-registry

working with `Schemas` :

```bash
$ curl --silent -X GET http://localhost:8081/schemas | jq
[
  {
    "subject": "musics-filepulse-csv2-value",
    "version": 1,
    "id": 1,
    "schema": "[\"null\",{\"type\":\"record\",\"name\":\"ConnectDefault\",\"namespace\":\"io.confluent.connect.avro\",\"fields\":[{\"name\":\"title\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"album\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"duration\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"release\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"artist\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"type\",\"type\":[\"null\",\"string\"],\"default\":null}]}]"
  },
....

$ curl --silent -X GET http://localhost:8081/schemas/ids/1 | jq -r .schema
["null",{"type":"record","name":"ConnectDefault","namespace":"io.confluent.connect.avro","fields":[{"name":"title","type":["null","string"],"default":null},{"name":"album","type":["null","string"],"default":null},{"name":"duration","type":["null","string"],"default":null},{"name":"release","type":["null","string"],"default":null},{"name":"artist","type":["null","string"],"default":null},{"name":"type","type":["null","string"],"default":null}]}]

$ curl --silent -X GET http://localhost:8081/schemas/ids/1 | jq ' .schema | fromjson'
[
  "null",
  {
    "type": "record",
    "name": "ConnectDefault",
    "namespace": "io.confluent.connect.avro",
    "fields": [
      {
        "name": "title",
        "type": [
          "null",
          "string"
        ],
        "default": null
      },
      {
        "name": "album",
        "type": [
          "null",
          "string"
        ],
        "default": null
      },
      {
        "name": "duration",
        "type": [
          "null",
          "string"
        ],
        "default": null
      },
     ....
```

##### Working With Subjects

```bash
$ curl --silent -X GET http://localhost:8081/subjects/ | jq .
[
  "musics-filepulse-csv-value",
  "musics-filepulse-csv2-value",
  "musics-filepulse-csv20-value"
]

$  curl --silent -X GET http://localhost:8081/subjects/musics-filepulse-csv20-value/versions/latest | jq .
{
  "subject": "musics-filepulse-csv20-value",
  "version": 1,
  "id": 2,
  "schema": "[\"null\",{\"type\":\"record\",\"name\":\"ConnectDefault\",\"namespace\":\"io.confluent.connect.avro\",\"fields\":[{\"name\":\"title\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"album\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"duration\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"artist\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"type\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"release\",\"type\":[\"null\",\"int\"],\"default\":null}]}]"
}


$ curl --silent -X GET http://localhost:8081/subjects/musics-filepulse-csv20-value/versions/latest | jq -r '.schema | fromjson'
[
  "null",
  {
    "type": "record",
    "name": "ConnectDefault",
    "namespace": "io.confluent.connect.avro",
    "fields": [
      {
        "name": "title",
        "type": [
          "null",
          "string"
        ],
        "default": null
      },
      {
        "name": "album",
        "type": [
          "null",
          "string"
        ],
        "default": null
      },
...
```

##### Registering a Schema 

we have a  schema for payment records as :

```json
{
 "namespace": "io.confluent.examples.clients.basicavro",
 "type": "record",
 "name": "Payment",
 "fields": [
     {"name": "id", "type": "string"},
     {"name": "amount", "type": "double"}
 ]
}
```

we can register it calling`POST` method :

```bash
$  curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data '{"schema": "{\"type\":\"record\",\"name\":\"Payment\",\"namespace\":\"io.confluent.examples.clients.basicavro\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"amount\",\"type\":\"double\"}]}"}' \
  http://localhost:8081/subjects/test-value/versions
  
Output :
{"id":3}

$ curl --silent -X GET http://localhost:8081/subjects/test-value/versions/latest | jq -r '.schema | fromjson'
{
  "type": "record",
  "name": "Payment",
  "namespace": "io.confluent.examples.clients.basicavro",
  "fields": [
    {
      "name": "id",
      "type": "string"
    },
    {
      "name": "amount",
      "type": "double"
    }
  ]
}  
  
```

##### Evolving Schema

```bash
4 curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data '{"schema": "{\"type\":\"record\",\"name\":\"Payment\",\"namespace\":\"io.confluent.examples.clients.basicavro\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"region\",\"type\":\"string\", \"default\":\"\"},{\"name\":\"amount\",\"type\":\"double\"}]}"}' \
  http://localhost:8081/subjects/test-value/versions

Output :
{"id":4}

$ curl --silent -X GET http://localhost:8081/subjects/test-value/versions/latest | jq -r '.schema | fromjson'
{
  "type": "record",
  "name": "Payment",
  "namespace": "io.confluent.examples.clients.basicavro",
  "fields": [
    {
      "name": "id",
      "type": "string"
    },
    {
      "name": "region",
      "type": "string",
      "default": ""
    },
    {
      "name": "amount",
      "type": "double"
    }
  ]
}

$ curl --silent -X GET http://localhost:8081/schemas/ids/4 | jq ' .schema | fromjson'
{
  "type": "record",
  "name": "Payment",
  "namespace": "io.confluent.examples.clients.basicavro",
  "fields": [
    {
      "name": "id",
      "type": "string"
    },
    {
      "name": "region",
      "type": "string",
      "default": ""
    },
    {
      "name": "amount",
      "type": "double"
    }
  ]
}

```


#### Working with MariaDB/Postgresql

- add MariaDB/Postgresql to compose file -> `docker-compose-v2`
- Create some sample  related tables in MariaDB/Postgres -> `airport data`
- Create appropriate `catalogs` in TrinO
- restart the cluster or use the `Trino-Admin`
- Use  trino cli to check the recent changes .
- Use  `DBeaver` for query the both databases!
- Check the Query Plan in `Web UI` section


#### Working with official Trino docker image

in this step,  we setup a single node Trino Cluster and work around with it :

 ```bash
 docker-compose -f .\docker-compose-v1.yml up
 ```

go to : http://localhost:8080

#### Default Configs & Catalogs

```bash
$ docker exec -it trino bash

$ ls -lh  /etc/trino/
drwxrwxrwx 1 root  root  4.0K Feb 28 15:47 catalog
-rw-r--r-- 1 trino trino  178 Feb 10 14:51 config.properties
-rw-r--r-- 1 trino trino  375 Feb 10 14:51 jvm.config
-rw-r--r-- 1 trino trino   52 Feb 10 14:51 log.properties
-rw-r--r-- 1 trino trino   50 Feb 10 14:51 node.properties

$ cat /etc/trino/config.properties
#single node install config
coordinator=true
node-scheduler.include-coordinator=true
http-server.http.port=8080
discovery-server.enabled=true
discovery.uri=http://localhost:8080

$ cat /etc/trino/node.properties
node.environment=docker
node.data-dir=/data/trino

$ ls -lh  /etc/trino/catalog/
total 0
-rwxrwxrwx 1 root  root  26 Jan 23 12:20 blackhole.properties
-rwxrwxrwx 1 root  root  20 Jan 23 12:20 jmx.properties
-rwxrwxrwx 1 root  root  21 Jan 23 12:20 memory.properties
-rw-r--r-- 1 trino trino 45 Feb 28 15:47 tpcds.properties
-rw-r--r-- 1 trino trino 43 Feb 28 15:47 tpch.properties


$ cat /etc/trino/catalog/memory.properties
connector.name=memory

$ cat /etc/trino/catalog/jmx.properties
connector.name=jmx


```



#### Working with Trino-CLI  :

######  Chapter 3

you can download the trino-cli JAR file from  [Central Repository: io/trino/trino-cli (apache.org)](https://repo.maven.apache.org/maven2/io/trino/trino-cli/) 

it's already included in this image. so let  get started with it : 

```bash
$ trino --version
Trino CLI 352

$ trino --server localhost:8080
trino> exit
$ trino
trino> help
QUIT
EXPLAIN [ ( option [, ...] ) ] <query>
    options: FORMAT { TEXT | GRAPHVIZ | JSON }
             TYPE { LOGICAL | DISTRIBUTED | VALIDATE | IO }
DESCRIBE <table>
SHOW COLUMNS FROM <table>
SHOW FUNCTIONS
SHOW CATALOGS [LIKE <pattern>]
SHOW SCHEMAS [FROM <catalog>] [LIKE <pattern>]
SHOW TABLES [FROM <schema>] [LIKE <pattern>]
USE [<catalog>.]<schema>


trino> show catalogs;
  Catalog
-----------
 blackhole
 jmx
 memory
 system
 tpcds
 tpch
(6 rows)

Query 20210228_174038_00001_c2ryz, FINISHED, 1 node
Splits: 19 total, 19 done (100.00%)
1.45 [0 rows, 0B] [0 rows/s, 0B/s]

trino> show schemas from tpch;

 Schema
--------------------
 information_schema
 sf1
 sf100
 sf1000
 sf10000
 sf100000
 sf300
 sf3000
 sf30000
 tiny
(10 rows)

Query 20210228_174235_00002_c2ryz, FINISHED, 1 node
Splits: 19 total, 19 done (100.00%)
2.24 [10 rows, 119B] [4 rows/s, 53B/s]

trino> SHOW TABLES FROM tpch.tiny;

  Table
----------
 customer
 lineitem
 nation
 orders
 part
 partsupp
 region
 supplier
(8 rows)

Query 20210228_174444_00004_c2ryz, FINISHED, 1 node
Splits: 19 total, 19 done (100.00%)
0.37 [8 rows, 166B] [21 rows/s, 450B/s]


```



**go to http://localhost:8080**

- Click `Finished` in filter area 
- Click on the `ID` of top(last) Query.
- See the details



##### Specify Catalog.Shema 

```bash
use tpch.tiny;
USE
trino:tiny> show tables;
  Table
----------
 customer
 lineitem
 nation
 orders
 part
 partsupp
 region
 supplier
(8 rows)

Query 20210228_181448_00012_c2ryz, FINISHED, 1 node
Splits: 19 total, 19 done (100.00%)
0.26 [8 rows, 166B] [31 rows/s, 651B/s]

trino:tiny> exit

$ trino --catalog tpch --schema sf1
trino:sf1>
```



##### Debug Mode / Explain 

```bash
$ trino --catalog tpch --schema sf1 --debug
trino:sf1> select count(*) from orders;
  _col0
---------
 1500000
(1 row)

Query 20210228_182931_00023_c2ryz, FINISHED, 1 node
http://localhost:8080/ui/query.html?20210228_182931_00023_c2ryz
Splits: 21 total, 21 done (100.00%)
CPU Time: 3.1s total,  482K rows/s,     0B/s, 91% active
Per Node: 3.4 parallelism, 1.64M rows/s,     0B/s
Parallelism: 3.4
Peak Memory: 0B
0.91 [1.5M rows, 0B] [1.64M rows/s, 0B/s]

trino:sf1> explain  select count(*) from orders;

-----------------------------------------------------------------------------
 Fragment 0 [SINGLE]
     Output layout: [count]
     Output partitioning: SINGLE []
     Stage Execution Strategy: UNGROUPED_EXECUTION
     Output[_col0]
     │   Layout: [count:bigint]
     │   Estimates: {rows: ? (?), cpu: ?, memory: ?, network: ?}
     │   _col0 := count
     └─ Aggregate(FINAL)
        │   Layout: [count:bigint]
        │   Estimates: {rows: ? (?), cpu: ?, memory: ?, network: ?}
        │   count := count("count_0")
        └─ LocalExchange[SINGLE] ()
           │   Layout: [count_0:bigint]
           │   Estimates: {rows: ? (?), cpu: ?, memory: ?, network: ?}
           └─ RemoteSource[1]
                  Layout: [count_0:bigint]

 Fragment 1 [tpch:orders:1500000]
     Output layout: [count_0]
     Output partitioning: SINGLE []
     Stage Execution Strategy: UNGROUPED_EXECUTION
     Aggregate(PARTIAL)
     │   Layout: [count_0:bigint]
     │   count_0 := count(*)
     └─ TableScan[tpch:orders:sf1.0, grouped = false]
            Layout: []
            Estimates: {rows: 1500000 (0B), cpu: 0, memory: 0B, network: 0B}
            tpch:orderstatus
                :: [[F], [O], [P]]


(1 row)

Query 20210228_183559_00036_c2ryz, FINISHED, 1 node
Splits: 1 total, 1 done (100.00%)
0.23 [0 rows, 0B] [0 rows/s, 0B/s]
```



#####  Pagination

```bash
trino:sf1> select * from orders limit 100;
 orderkey | custkey | orderstatus | totalprice | orderdate  |  orderpriority  |      clerk      | shippriority |                                   comment
----------+---------+-------------+------------+------------+-----------------+-----------------+--------------+--------------------------------------------
  3000001 |  145618 | F           |   30175.88 | 1992-12-17 | 4-NOT SPECIFIED | Clerk#000000141 |            0 | l packages. furiously careful instructions
  3000002 |    1481 | O           |  297999.63 | 1995-07-28 | 1-URGENT        | Clerk#000000547 |            0 | carefully unusual dependencie
  3000003 |  127432 | O           |  345438.38 | 1997-11-04 | 5-LOW           | Clerk#000000488 |            0 | n packages boost slyly bold deposits. depos
  3000004 |   47423 | O           |  135965.53 | 1996-06-13 | 4-NOT SPECIFIED | Clerk#000000004 |            0 | nts wake carefully final decoys. quickly fi
  3000005 |   84973 | F           |  209937.09 | 1992-09-12 | 5-LOW           | Clerk#000000030 |            0 | yly after the quickly unusual ide
  
  
```

- **Enter ** : down one row
- **Up / Down** : Scrolling One Row at a time.
- **Pg Down/Pg Up** : Scroll down/up
- **q** : Quit



##### Execute Queries & Save the Result  in desired format 

###### Page : 51

```bash 
# in linux bash 
$ cd 
$ trino --execute "SELECT nation.name AS nation, region.name AS region \
FROM tpch.sf1.region, tpch.sf1.nation \
WHERE region.regionkey = nation.regionkey \
AND region.name LIKE 'AFRICA';"

$ trino --execute "SELECT *from tpch.sf1.nation" --output-format CSV_HEADER > nations.csv

$ trino --execute "SELECT *from tpch.sf1.nation" --output-format ALIGNED 
 nationkey |      name      | regionkey |                                                      comment

-----------+----------------+-----------+--------------------------------------------------------------------------------------------------------------------
         0 | ALGERIA        |         0 |  haggle. carefully final deposits detect slyly agai

         1 | ARGENTINA      |         1 | al foxes promise slyly according to the regular accounts. bold requests alon


$ trino --execute "SELECT *from tpch.sf1.nation" --output-format VERTICAL
-[ RECORD 1 ]-----------------------------------------------------------------------------------------------------------------
nationkey | 0
name      | ALGERIA
regionkey | 0
comment   |  haggle. carefully final deposits detect slyly agai
-[ RECORD 2 ]-----------------------------------------------------------------------------------------------------------------
nationkey | 1
name      | ARGENTINA
regionkey | 1
comment   | al foxes promise slyly according to the regular accounts. bold requests alon
```



##### Load Sample Data 

```bash
$ head -15 /home/trino/data/iris-data-set/iris-data-set.sql
USE memory.default;
CREATE TABLE iris (
  sepal_length_cm real,
  sepal_width_cm real,
  petal_length_cm real,
  petal_width_cm real,
  species varchar(10)
);
INSERT INTO iris ( sepal_length_cm, sepal_width_cm, petal_length_cm,
  petal_width_cm, species )
VALUES
    ( 5.1,  3.5 ,  1.4 ,  0.2 , 'setosa' ),
    ( 4.9 ,  3.0 ,  1.4 ,  0.2 , 'setosa' ),
    ( 4.7 ,  3.2 ,  1.3 ,  0.2 , 'setosa' ),
    ( 4.6 ,  3.1 ,  1.5 ,  0.2 , 'setosa' ),
 ....
$  trino -f /home/trino/data/iris-data-set/iris-data-set.sql
USE
CREATE TABLE
INSERT: 150 rows

$ trino
trino> use memory.default;
USE
trino:default> show tables;
 Table
-------
 iris
(1 row)

trino:default> select * from iris limit 10;
 sepal_length_cm | sepal_width_cm | petal_length_cm | petal_width_cm | species
-----------------+----------------+-----------------+----------------+---------
             5.1 |            3.5 |             1.4 |            0.2 | setosa
             4.9 |            3.0 |             1.4 |            0.2 | setosa
```



##### Working with Trino/Presto  using DBeaver

you can work with presto/trino using DBeaver.

let's try it!


import time, re
import requests, json
from elasticsearch import Elasticsearch

es = Elasticsearch(http_auth=('elastic', 'changeme'))

url = 'https://www.sahamyab.com/guest/twiter/list?v=0.1'
count_needed, sleep_time = 1000, 60
count_sofar = 0
regex = r"#([^\s#]+)"
tweet_ids = set()


while count_sofar < count_needed:
    response = requests.request('GET', url, headers={'User-Agent': 'Chrome/61'})
    result = response.status_code
    if result == requests.codes.ok:
        tweets = response.json()['items']
        for tweet in tweets:
            try:
                if tweet["id"] not in tweet_ids :
                    # extracting the hashtags
                    hashtag_list = re.findall(regex, tweet["content"])
                    tweet['hashtags'] = hashtag_list
                    res = es.index(index="tweets", id=tweet["id"], body=tweet)
                    print(f"tweet ID : {tweet['id']}")
                    count_sofar += 1
                    tweet_ids.add(tweet["id"])
                
            except Exception as e:
                print("print exception: " + str(e))
            
    else:
        print("Response code error: " + str(result))
    print(f'Count of fetched tweets is {count_sofar}')
    time.sleep(sleep_time)

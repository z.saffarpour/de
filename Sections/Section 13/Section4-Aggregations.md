##### A Sample Aggregation

- `aggs` can be used within a normal query . 

```json
GET /my-index-000001/_search
{
  "aggs": {
    "my-agg-name": {
      "terms": {
        "field": "my-field"
      }
    }
  }
}
```

- `hashtags` statistics

```json
GET /tweets_fa/_search
{
    
  "aggs": {
    "a-sample-name": {
      "terms": {
        "field": "hashtags.keyword"
      }
    }
  }
}
```

- check out the bottom part of the result :

```json
"aggregations" : {
    "a-sample-name" : {
      "doc_count_error_upper_bound" : 0,
      "sum_other_doc_count" : 129,
      "buckets" : [
        {
          "key" : "شاخص_بورس",
          "doc_count" : 42
        },
        {
          "key" : "برکت",
          "doc_count" : 26
        },
        {
          "key" : "فملی",
          "doc_count" : 17
        },
        {
          "key" : "وبملت",
          "doc_count" : 10
        },
        {
          "key" : "شپلی",
          "doc_count" : 9
        },
        {
          "key" : "خودرو",
          "doc_count" : 8
        },
        {
          "key" : "شپنا",
          "doc_count" : 8
        },
        {
          "key" : "دارا_یکم",
          "doc_count" : 6
        },
        {
          "key" : "دماوند",
          "doc_count" : 6
        },
        {
          "key" : "فولاد",
          "doc_count" : 6
        }
      ]
    }
  }
```

- you can omit the data items :

```json
GET /tweets_fa/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "terms": {
        "field": "hashtags.keyword"
      }
    }
  }
}
```

- custom buckets :

```json
GET tweets_fa/_search
{
  "size": 0, 
  "aggs": {
    "bours": {
      "terms": {
        "field": "hashtags.keyword",
        "include": [ "شاخص_بورس"]
      }
    },
    "cars": {
      "terms": {
        "field": "hashtags.keyword",
        "include": [ "خودرو","خساپا" ]
      }
    }
  }
}
```



- another sample with `ordering`

```json

GET /tweets_fa/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "terms": {
        "field": "senderUsername",
        "size": 10,
        "shard_size": 20,
        "order": {
          "_count": "desc"
        }
        
      }
    }
  }
}
```



- last 2 days statistics ([date math rounding ](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html#range-query-date-math-rounding)):

```json
GET /tweets_fa/_search
{
  "size": 0,
  "query": {
    "range": {
      "sendTime": {
        "gte": "now-2d/d",
        "lte": "now/d"
      }
    }
  },
  "aggs": {
    "a-sample-name": {
      "terms": {
        "field": "hashtags.keyword"
      }
    }
  }
}
```

##### Specify Shard Bucket Size

```json
GET /tweets_fa/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "terms": {
        "field": "hashtags.keyword",
        "size": 5,
        "shard_size": 10
      }
    }
  }
}
```

- shard bucket size better to be 3 or 4 times more than query size.
- The default `shard_size` is `(size * 1.5 + 10)`

##### Date Histogram

- `day` interval

```json
GET /tweets_fa/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "date_histogram": {
        "field": "sendTime",
        "interval": "day",
        "format": "yyyy-MM-dd",
         "order": { "_count": "desc" }
      }
    }
  }
}


```

- `Hour` interval

```json
GET /tweets_fa/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "date_histogram": {
        "field": "scoredPostDate",
        "interval": "hour",
        "format": "yyyy-MM-dd:hh:mm",
         "order": { "_count": "desc" }
      }
    }
  }
}
```



##### Max/Stats Aggs

- max for a numeral data type

```json
GET /tweets_fa/_search
{
  "size": 0, 
  "aggs": {
    "like-count": {
      "terms": {
        "field": "likeCount",
        "order": {
          "max_like_count": "desc"
        }
      },
      "aggs": {
        "max_like_count": {
          "max": {
            "field": "likeCount"
          }
        }
      }
    }
  }
}
```

- `max`,`min`, `avg` , ...
- `stats` aggs

```json
GET /tweets_fa2/_search
{
  "size": 0, 
  "aggs": {
    "like-count": {
      "terms": {
        "field": "likeCount",
        "order": {
          "like_count_stats.max": "desc"
        }
      },
      "aggs": {
        "like_count_stats": {
          "stats": {
            "field": "likeCount"
          }
        }
      }
    }
  }
}
```

-  "order": { "_key": "desc"  }

```json
GET /tweets/_search
{
  "size": 0, 
  "aggs": {
    "like-count": {
      "terms": {
        "field": "hashtags.keyword",
        "order": {
          "like_count_stats.max": "desc"
        }
      },
      "aggs": {
        "like_count_stats": {
          "stats": {
            "field": "likeCount"
          }
        }
      }
    }
  }
}
```



#### Nested Aggs

```json
GET /tweets_fa2/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "date_histogram": {
        "field": "sendTime",
        "interval": "day",
        "format": "yyyy-MM-dd",
         "order": { "_count": "desc" }
      },
      "aggs": {
        "test": {
          "terms": {
            "field": "senderUsername",
            "size": 10
          }
        }
      }
    }
  }
}
```

- Filter out some result :

```json
GET /tweets_fa2/_search
{
  "size": 0,
  "aggs": {
    "a-sample-name": {
      "date_histogram": {
        "field": "sendTime",
        "interval": "day",
        "format": "yyyy-MM-dd",
        "order": {
          "_count": "desc"
        }
      },
      "aggs": {
        "test": {
          "filter": {
            "term": {
              "type": "twit"
            }
          },
          "aggs": {
            "nested": {
              "terms": {
                "field": "hashtags.keyword",
                "size": 10
              }
            }
          }
        }
      }
    }
  }
}
```

- it's simpler to do it this way :

```json
GET /tweets_fa2/_search
{
  "size": 0,
  "query": {
    "bool": {
      "filter": {
        "term": {
          "type": "twit"
        }
      }
    }
  },
  "aggs": {
    "a-sample-name": {
      "date_histogram": {
        "field": "sendTime",
        "interval": "day",
        "format": "yyyy-MM-dd",
        "order": {
          "_count": "desc"
        }
      },
      "aggs": {
        "test": {
          "terms": {
            "field": "hashtags.keyword",
            "size": 10
          }
        }
      }
    }
  }
}
```

##### Minimum document count

It is possible to only return terms that match more than a configured number of hits using the `min_doc_count` option:

```console
GET /_search
{
  "aggs": {
    "tags": {
      "terms": {
        "field": "tags",
        "min_doc_count": 10
      }
    }
  }
}
```


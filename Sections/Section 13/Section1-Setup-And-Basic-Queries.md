### 1- Installation

##### Option 1 : Install Elasticsearch Manually

***Download and unzip Elasticsearch on Windows*** 

Elasticsearch can also be installed from our package repositories **using apt or yum**, or installed on **Windows using an MSI** installer package. See [*Repositories* in the Guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html).

- Download Link - Elasticsearch (Zip File) : [Download Elasticsearch Free | Get Started Now | Elastic | Elastic](https://www.elastic.co/downloads/elasticsearch)

- Download Link- Kibana (Zip File) : [Download Kibana Free | Get Started Now | Elastic | Elastic](https://www.elastic.co/downloads/kibana)

- Unzip both files in the `elasticsearch`  directory . 

------

**Elasticsearch :**

- [Check out  that  Java is installed](https://dazhyarco.net/%D9%86%D8%B5%D8%A8-jdk/)
- Run `bin/elasticsearch` (or `bin\elasticsearch.bat` on Windows)

- Run `curl http://localhost:9200/` or `Invoke-RestMethod http://localhost:9200` with PowerShell or Check Out the Address with your favorite Browser.

----

**Kibana :** 

- Open `config/kibana.yml` in an editor
-  Set `elasticsearch.hosts` to point at your Elasticsearch instance
- Run `bin/kibana` (or `bin\kibana.bat` on Windows)
- Point your browser at http://localhost:5601

------

##### Option 2 : Docker

```bash
docker-compose up
```

### 2- Basic Commands

#### 2-1- Insert Document

```json
PUT /movies/movie/1
{
    "title": "The Godfather",
    "director": "Francis Ford Coppola",
    "year": 1972
}
```

- Warning : Types are deprecating!

**`#! [types removal] Specifying types in document index requests is deprecated, use the typeless endpoints instead (/{index}/_doc/{id}, /{index}/_doc, or /{index}/_create/{id}).`**

```json
PUT /movies/_doc/1
{
    "title": "The Godfather",
    "director": "Francis Ford Coppola",
    "year": 1972
}
```

- Version Updated.
- Only Valid JSON would be inserted.

- Random ID :

```json
POST /movies/_doc
{
    "title": "The Godfather",
    "director": "Francis Ford Coppola",
    "year": 1972
}
```

- id : *ImTDPXkBsTUAhNd6bph2*

> **usually we use POST to create a resource and PUT to modify that.**
>
> Deciding between POST and PUT is easy: use PUT if and only if the endpoint will follow these 2 rules:
>
> 1. The endpoint must be idempotent: so safe to redo the request over and over again;
> 2. The URI must be the address to the resource being updated.
>
> [Source](https://symfonycasts.com/screencast/rest/put-versus-post)

- issue these command from **Postman**
- try curl :

```bash
curl -H "Content-Type: application/json" -XPUT "localhost:9200/products/_doc/1?pretty" -d 
'
{
  "name": "iPhone 7",
  "camera": "12MP",
  "storage": "256GB",
  "display": "4.7inch",
  "battery": "1,960mAh",
  "reviews": ["Incredibly happy after having used it for one week", "Best iPhone so far", "Very expensive, stick to Android"]
  }
 '
```

  

#### Retrieve a Document

```json
GET /movies/_doc/1
GET /movies/_doc/ImTDPXkBsTUAhNd6bph2
GEt /movies/_doc/66
```

### Insert Some Documents

```json
PUT movies/_doc/2
{
    "title": "Lawrence of Arabia",
    "director": "David Lean",
    "year": 1962,
    "genres": ["Adventure", "Biography", "Drama"]
}
PUT movies/_doc/3
{
    "title": "To Kill a Mockingbird",
    "director": "Robert Mulligan",
    "year": 1962,
    "genres": ["Crime", "Drama", "Mystery"]
}
PUT movies/_doc/4
{
    "title": "Apocalypse Now",
    "director": "Francis Ford Coppola",
    "year": 1979,
    "genres": ["Drama", "War"]
}
PUT movies/_doc/5
{
    "title": "Kill Bill: Vol. 1",
    "director": "Quentin Tarantino",
    "year": 2003,
    "genres": ["Action", "Crime", "Thriller"]
}
PUT movies/_doc/6
{
    "title": "The Assassination of Jesse James by the Coward Robert Ford",
    "director": "Andrew Dominik",
    "year": 2007,
    "genres": ["Biography", "Crime", "Drama"]
}
```

#### Search

##### Query Structure

```json
POST movies/_search
{
    "query"{
    //Query DSL here
	}
}
```

- **_search endpoint**

- in `ES` documents, search queries issued by `GET` http method.

- **Elasticsearch allows three ways to perform a search request...**

  ###### GET with request body:

  ```
  curl -XGET "http://localhost:9200/app/users/_search" -d '{
    "query": {
      "term": {
        "email": "foo@gmail.com"
      }
    }
  }'
  ```

  ###### POST with request body:

  [Since not all clients support GET with body, POST is allowed as well.](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html#search-request-body)

  ```
  curl -XPOST "http://localhost:9200/app/users/_search" -d '{
    "query": {
      "term": {
        "email": "foo@gmail.com"
      }
    }
  }'
  ```

  ######  GET without request body:

  ```
  curl -XGET "http://localhost:9200/app/users/_search?q=email:foo@gmail.com"
  ```


#### [query_string](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html)

```json
POST movies/_doc/_search
{
    "query": {
        "query_string": {
            "query": "kill"
        }
    }
}
```

- `POST movies/_search`
- `POST _all/_search`
- [Query **DSL** (Domain Specific Language)](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html) 

- **Default Operator**
```
quick brown +fox -news
```

states that:

- `fox` must be present
- `news` must not be present
- `quick` and `brown` are optional — their presence increases the relevance
- Wild Cards Allowed : `qu?ck bro*` 
  - use them carefully!



#### Query String - Specify Fields

```json
POST _all/_search
{
    "query": {
    		"query_string": {
   					 "query": "ford",
  					 "fields": ["title"]
    		}
    }
}
```


##### Query String - Boolean Queries

```json
"query_string" : {"query" : "title:ford"}

"query_string" : {"query" : "title:ford OR kill"}

"query_string" : { "query": "(title:kill) AND (director:Tarantino)" }

"query_string" : { "query":"(genres:Drama) AND (year:1962 )" }

"query_string" : { "query":"(genres:Drama) AND (year:[* TO 2005] )" }

"query_string" : {"query": "(genres:Drama) OR (title:kill)" }

```

###### Boosting

```json
"query_string" : {"query": "(genres:Drama) OR (title:kill^2)" }
```

###### Search Inside fields 

```json
GET /_search
{
  "query": {
    "query_string" : {
      "fields" : ["city.*"],
      "query" : "this AND that OR thus"
    }
  }
}


PUT movies/_doc/3
{
    "title": "To Kill a Mockingbird",
    "director": "Robert Mulligan",
    "year": 1962,
    "genres": ["Crime", "Drama", "Mystery"],
    "city":{
      "name":"new york",
      "location":"us",
      "weather":"normal"
    }
}

GET /_search
{
  "query": {
    "query_string": {
      "fields": [
        "city.*"
      ],
      "query": "york AND us OR test"
    }
  }
}
```

#### [How `minimum_should_match` works for multiple fields](https://github.com/elastic/elasticsearch/edit/7.12/docs/reference/query-dsl/query-string-query.asciidoc)

```console
GET /_search
{
  "query": {
    "query_string": {
      "fields": [
        "title"
        ],
      "query": "new york kill bill",
      "minimum_should_match": 2
    }
  }

```

#### Building A Search Tree

- [Match Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html)

```jso
POST /movies/_search
{
  "query": {
    "match": {
      "genres": "drama"
    }
  }
}

or  

POST /movies/_search
{
  "query": {
    "match": {
      "genres": {
        "query": "drama"
      }
    }
  }
}
```

- Inner Query would be Analyzed , then checked for matching
- default operator is `OR`

```json
POST movies/_search
{
  "query": {
    "match": {
      "title": {
        "query": "Kill Bill",
        "operator": "or"
      }
    }
  }
}
```

- **Multi Match**

```json
POST /movies/_search
{
  "query": {
    "multi_match": {
      "query": "ford",
      "fields": [
        "title^3",
        "director"
      ]
    }
  }
}
```

#### Boolean Queries

- **must** : And
- **should ** : Or
- **must_not **: not

```jso
POST /movies/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "genres": "drama"
          }
        },
        {
          "match": {
            "title": {
              "query":"kill",
               "boost": 3
            }
          }
        }
      ]
    }
  }
}
```

- An Advanced Sample : (**title** contains ***ford*** or ***kill***) and (**genres** is not ***mystery***)

```json
POST /movies/_search
{
  "query": {
    "bool": {
      "must": {
        "bool": {
          "should": [
            {
              "match": {
                "title": "ford"
              }
            },
            {
              "match": {
                "title": "kill"
              }
            }
          ]
        }
      },
      "must_not": {
        "match": {
          "genres": "Mystery"
        }
      }
    }
  }
}
```

- every bool section must contains only one of the ***must, should, must_not***

#### [Term Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-term-query.html)

- Returns documents that contain an **exact** term in a provided field.
- Avoid using the `term` query for [`text`](https://www.elastic.co/guide/en/elasticsearch/reference/current/text.html) fields.

```bash
GET /_search
{
  "query": {
    "term": {
      "year": {
        "value": 2003,
        "boost": 1.0
      }
    }
  }
}
```

- Use term query for text fields if they are normally consists of ***one word***  or are ***keywords***

#### Filtering the Results 

-  for **nontext fields**, ES *first apply the filters, then run the text retrieval process*.

```json
POST /movies/movie/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "title": "kill"
          }
        }
      ],
      "filter": {
        "bool": {
          "must": [
            {
              "range": {
                "year": {
                  "gte": 1960
                }
              }
            },
            {
              "term": {
                "genres": {
                  "value": "drama"
                }
              }
            }
          ]
        }
      }
    }
  }
}
```

- Our queries have normally 2 parts : ***filters and text queries***
- we can use filtering parts inside the Boolean queries but it's better to use it alongside the `must`,`should` & `must_not` :

```json

POST /movies/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "title": "kill"
          }
        },
        {
          "range": {
            "year": {
              "gte": 1960
            }
          }
        },
        {
          "term": {
            "genres": {
              "value": "drama"
            }
          }
        }
      ]
    }
  }
}
```

- what if we have not test query, at all ?

```json
POST /movies/_search
{
  "query": {
    "bool": {
      "must": [
     	{
            "match_all": {}
        }
      ],
      "filter": {
        "bool": {
          "must": [
            {
              "range": {
                "year": {
                  "gte": 1960
                }
              }
            },
            {
              "term": {
                "genres": {
                  "value": "drama"
                }
              }
            }
          ]
        }
      }
    }
  }
}


or

POST movies/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "year": {
              "gte": 2000
            }
          }
        },
        {
          "term": {
            "genres": {
              "value": "drama"
            }
          }
        }
      ]
    }
  }
}
```

- why this query have no results ? 

```json
POST movies/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "year": {
              "gte": 2000
            }
          }
        },
        {
          "term": {
            "genres": {
              "value": "Drama"
            }
          }
        }
      ]
    }
  }
}
```

#### Fuzziness

- fuzziness  can resist ***spelling errors***

```json
POST /movies/_search
{
  "query": {
    "match": {
      "title": {
        "query": "godfater",
        "fuzziness": "AUTO"
      }
    }
  }
}
```

fuzziness is based on the ***edit distance*** . An edit distance is the number of one-character changes needed to turn one term into another. These changes can include:

- Changing a character (**b**ox → **f**ox)
- Removing a character (**b**lack → lack)
- Inserting a character (sic → sic**k**)
- Transposing two adjacent characters (**ac**t → **ca**t)

- [Fuzzy Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-fuzzy-query.html) have more parameters to fit your needs.

```json
GET /_search
{
  "query": {
    "fuzzy": {
      "title": {
        "value": "goodfathr"
      }
    }
  }
}
```

#### Phrase Search

```json
POST /movies/_search
{
  "query": {
    "match_phrase": {
      "title": {
        "query": "Kill Bill",
        "slop": 2
      }
    }
  }
}

```

- A phrase query matches terms up to a configurable `slop` (Word Distance)



#### Keywords

- why this query has no result ? 

```json
POST movies/_search
{
  "query": {
    "term": {
      "director": {
        "value": "Francis Ford Coppola"
      }
    }
  }
}
```

or why this query has result?

```json
POST movies/_search
{
  "query": {
    "match": {
      "director": {
        "query" : "francis ford coppola",
        "operator" : "and" 
      }
    }
  }
}
```

or why this query has result?

```json
POST /movies/_search
{
  "query": {
    "match_phrase": {
      "director": {
        "query": "francis ford coppola",
        "slop": 3
      }
    }
  }
}
```

- the answer is : ***ES Analyzes Text/Queries*** before Indexing/Retrieving them  

- to retrieve the ***Exact Phrase*** , use ***Keywords*** version of text fields that not analyzed.

```json
POST movies/_search
{
    "query": {
      "term": {
        "director.keyword": {
          "value": "Francis Ford Coppola"
        }
      }
    }
}
```

or 

```json
POST /movies/_search
{
  "query": {
    "match_phrase": {
      "director.keyword": {
        "query": "francis ford coppola",
        "slop": 3
      }
    }
  }
}
```



#### [ID Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-ids-query.html) 

Returns documents based on their IDs

```json
GET movies/_search
{
  "query": {
    "ids" : {
      "values" : ["1", "4"]
    }
  }
}
```


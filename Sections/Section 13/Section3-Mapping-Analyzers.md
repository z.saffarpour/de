#### Mapping

Mapping is the process of defining how a document, and the fields it contains, are stored and indexed. When mapping your data, you create a mapping definition, **which contains a list of fields and their types** that are pertinent to the document.

- *dynamic mapping*
- *explicit mapping*

##### tweets type

```json
PUT tweets/_doc/281045972
{
  "id": "281045972",
  "sendTime": "2021-05-07T07:46:42Z",
  "sendTimePersian": "1400/02/17 12:16",
  "senderName": "SIAVASH 1845",
  "senderUsername": "sia1845",
  "senderProfileImage": "59f83cb3-42db-4678-aaec-e2670a77ad48",
  "content": "#برکت جمع کنین آقا .. هدف این هدف ما اون .. توجه همه دوستان رو به چهار خط آخر خبر که دوستمون زحمت کشیده جلب میکنم ... به این میگن مصادر به مطلوب از موضوع .. ",
  "type": "twit",
  "scoredPostDate": "1620373609610",
  "finalPullDatePersian": ""
}
```

##### get mapping of tweets

```json
GET /tweets/_mappings

Result : 

{
  "tweets" : {
    "mappings" : {
      "properties" : {
        "content" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "finalPullDatePersian" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "id" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "scoredPostDate" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "sendTime" : {
          "type" : "date"
        },
        "sendTimePersian" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "senderName" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "senderProfileImage" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "senderUsername" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "type" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        }
      }
    }
  }
}

```

##### correct the mapping : 

*Data Types* : [see the documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)

*Date Type* : [see the various formats](https://www.elastic.co/guide/en/elasticsearch/reference/current/date.html)

```json
PUT /tweets/_mapping
{
  "properties": {
    "content": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      }
    },
    "finalPullDatePersian": {
      "type": "date"
    },
    "id": {
      "type": "keyword"
    },
    "scoredPostDate": {
      "type": "date",
      "format": "epoch_millis||epoch_second"
    },
    "sendTime": {
      "type": "date",
      "format":"date_time_no_millis||strict_date_time_no_millis"
    },
    "sendTimePersian": {
      "type": "keyword"
    },
    "senderName": {
      "type": "keyword"
    },
    "senderProfileImage": {
      "type": "keyword"
    },
    "senderUsername": {
      "type": "keyword"
    },
    "type": {
      "type": "keyword"
    }
  }
}
```

Error Occurred : 

```bash
"mapper [senderProfileImage] cannot be changed from type [text] to [keyword]"
```

##### reindex the tweets

- create an empty index and set `settings` & `mappings`

```json
PUT /tweets_temp/
{
  "settings": {
    "index.mapping.ignore_malformed": true
  },
  "mappings": {
    "properties": {
      "content": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "finalPullDatePersian": {
        "type": "date"
      },
      "id": {
        "type": "keyword"
      },
      "scoredPostDate": {
        "type": "date",
        "format": "epoch_millis||epoch_second"
      },
      "sendTime": {
        "type": "date",
        "format": "date_time_no_millis||strict_date_time_no_millis"
      },
      "sendTimePersian": {
        "type": "keyword"
      },
      "senderName": {
        "type": "keyword"
      },
      "senderProfileImage": {
        "type": "keyword"
      },
      "senderUsername": {
        "type": "keyword"
      },
      "type": {
        "type": "keyword"
      }
    }
  }
}
```

- reindex the tweets

```json
POST _reindex
{
  "source": {
    "index": "tweets"
  },
  "dest": {
    "index": "tweets_temp"
  }
}
```

- check the memory usage :

```json
GET tweets/_stats
GET tweets_temp/_stats
```

- rename the `tweets_temp` to `tweets`

```json
DELETE tweets

PUT /tweets
{
  "settings": {
    "index.mapping.ignore_malformed": true
  },
  "mappings": {
    "properties": {
      "content": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "finalPullDatePersian": {
        "type": "date"
      },
      "id": {
        "type": "keyword"
      },
      "scoredPostDate": {
        "type": "date",
        "format": "epoch_millis||epoch_second"
      },
      "sendTime": {
        "type": "date",
        "format": "date_time_no_millis||strict_date_time_no_millis"
      },
      "sendTimePersian": {
        "type": "keyword"
      },
      "senderName": {
        "type": "keyword"
      },
      "senderProfileImage": {
        "type": "keyword"
      },
      "senderUsername": {
        "type": "keyword"
      },
      "type": {
        "type": "keyword"
      }
    }
  }
}

POST /_reindex
{
  "source": {
    "index": "tweets_temp"
  },
  "dest": {
    "index": "tweets"
  }
}

DELETE tweets_temp
```

- this work if the mapping has `_source: {enabled: true}` - default 
- we can use `Clone` [endpoint](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-clone-index.html) , but it requires to disable `write` operations.

##### quote type

```json
PUT tweets/_doc/281046023
{
  "id": "281046023",
  "sendTime": "2021-05-07T07:47:27Z",
  "sendTimePersian": "1400/02/17 12:17",
  "parentSendTime": "2021-05-07T07:39:47Z",
  "parentSendTimePersian": "1400/02/17 12:09",
  "parentId": "281045659",
  "parentSenderName": "سیاوش",
  "parentSenderUsername": "hate8fereydon",
  "parentSenderProfileImage": "b2e9a69d-2f98-4223-aa2c-98d7c3c7dfb4",
  "parentContent": """#شاخص_بورس 😑😑😑😑😑😑
بورس تله شد برای آتش زدن نقدینگی... 
متاسفانه هرکجا بودیم باید از صفر شروع کنیم. 
فقط نباید فراموش کنیم.""",
  "senderName": "مهدی",
  "senderUsername": "hbidmeshk",
  "senderProfileImage": "81faba73-85a3-4714-90fc-82c39dd99c61",
  "content": """#شپنا نیویورک تایمز: ممکن است برجام در این دوره از مذاکرات که جمعه آغاز می شود، احیا شود

🔹یک مقام ارشد وزارت امور خارجه آمریکا ،روز پنجشنبه، در آستانه این دور از مذاکرات که می تواند دور نهایی مذاکرات قبل از  توافق باشد، گفت ایالات متحده و ایران  می توانند ظرف چند هفته به پایبندی توافق هسته ای سال 2015 بازگردند.
🔹به گزارش نیویورک تایمز، این خوش بینانه ترین سیگنال دولت بایدن است که می گوید بازگشت آمریکایی ها به توافق بین ایران و قدرت های جهانی در دسترس است.
🔹این مقام ارشد که خواست نامش فاش نشود در جمع خبرنگاران گفت، توافق قبل از انتخابات ریاست جمهوری ایران در اواسط ژوئن  هم ممکن و هم قابل انجام است. وی گفت این توافق به طور بالقوه می تواند در  این دوره از مذاکرات که از روز جمعه در وین آغاز می شود، انجام شود.""",
  "type": "quote",
  "finalPullDatePersian": ""
}
```

- Mapping is Changed !

###### How to Define  multiple mapping for an Index ?

> for storing multiple types in one index that are somehow similar such as `tweets` a `retweets` , we can define  a complete mapping that contains the fields of both types . a field that stores the `type` would be added the `mapping`  . we usually name it `type`
>
> in search endpoint,  we filter result based on this new `type` field.

1- define new mapping :

```json
PUT /tweets_temp/
{
  "settings": {
    "index.mapping.ignore_malformed": true
  },
  "mappings": {
    "properties": {
      "content": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "finalPullDatePersian": {
        "type": "date"
      },
      "id": {
        "type": "keyword"
      },
      "scoredPostDate": {
        "type": "date",
        "format": "epoch_millis||epoch_second"
      },
      "sendTime": {
        "type": "date",
        "format": "date_time_no_millis||strict_date_time_no_millis"
      },
      "sendTimePersian": {
        "type": "keyword"
      },
      "senderName": {
        "type": "keyword"
      },
      "senderProfileImage": {
        "type": "keyword"
      },
      "senderUsername": {
        "type": "keyword"
      },
      "type": {
        "type": "keyword"
      },
      "parentSendTimePersian": {
        "type": "keyword"
      },
      "parentId": {
        "type": "keyword"
      },
      "parentSenderName": {
        "type": "keyword"
      },
      "parentSenderUsername": {
        "type": "keyword"
      },
      "parentSenderProfileImage": {
        "type": "keyword"
      },
       "likeCount": {
        "type": "integer"
      },
      "retwitCount": {
        "type": "integer"
      },
      "parentContent": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
```

2- reindex & rename :

```json
POST _reindex
{
  "source": {
    "index": "tweets"
  },
  "dest": {
    "index": "tweets_temp"
  }
}

DELETE tweets

PUT /tweets/
{
  "settings": {
    "index.mapping.ignore_malformed": true
  },
  "mappings": {
    "properties": {
      "content": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "finalPullDatePersian": {
        "type": "date"
      },
      "id": {
        "type": "keyword"
      },
      "scoredPostDate": {
        "type": "date",
        "format": "epoch_millis||epoch_second"
      },
      "sendTime": {
        "type": "date",
        "format": "date_time_no_millis||strict_date_time_no_millis"
      },
      "sendTimePersian": {
        "type": "keyword"
      },
      "senderName": {
        "type": "keyword"
      },
      "senderProfileImage": {
        "type": "keyword"
      },
      "senderUsername": {
        "type": "keyword"
      },
      "type": {
        "type": "keyword"
      },
      "parentSendTimePersian": {
        "type": "keyword"
      },
      "parentId": {
        "type": "keyword"
      },
      "parentSenderName": {
        "type": "keyword"
      },
      "parentSenderUsername": {
        "type": "keyword"
      },
      "parentSenderProfileImage": {
        "type": "keyword"
      },
       "likeCount": {
       "type": "integer"
      },
      "retwitCount": {
        "type": "integer"
      },
      "parentContent": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}

POST /_reindex
{
  "source": {
    "index": "tweets_temp"
  },
  "dest": {
    "index": "tweets"
  }
}

DELETE tweets_temp

```



- filter by type :

```json

GET tweets/_search
{
  "query": {
    "bool": {
      "filter": {
        "match": {
          "type": "twit"
        }
      }
    }
  }
}

OR

GET tweets/_search
{
  "query": {
    "match": {
      "type": "twit"
    }
  }
}
```



#### [Using Ingest Pipelines](https://www.elastic.co/guide/en/elasticsearch/reference/current/ingest.html)

we can modify our documents before indexed and stored in ES using ingestion pipelines.

![Ingest pipeline diagram](https://www.elastic.co/guide/en/elasticsearch/reference/current/images/ingest/ingest-process.svg)

- we want to add @timestamp to `tweets` index 
  - first create a new ingestion pipeline :

  ```json
  PUT _ingest/pipeline/add-current-time
  {
    "description" : "automatically add the current time to the documents",
    "processors" : [
      {
        "set" : {
          "field": "@timestamp",
          "value": "{{{_ingest.timestamp}}}"
        }
      }
    ]
  }
  ```
  
  -  then use it when inserting data 
  
  ```json
  PUT tweets/_doc/323232?pipeline=add-current-time
  {
    "id": "323232",
    "sendTime": "2021-05-07T07:46:42Z",
    "sendTimePersian": "1400/02/17 12:16",
    "senderName": "SyuIAVASH 1845",
    "senderUsername": "sia1845",
    "senderProfileImage": "59f83cb3-42db-4678-aaec-e2670a77ad48",
    "content": "#برکت جمع کنین آقا .. هدف این هدف ما اون .. توجه همه دوستان رو به چهار خط آخر خبر که دوستمون زحمت کشیده جلب میکنم ... به این میگن مصادر به مطلوب از موضوع .. ",
    "type": "twit",
    "scoredPostDate": "1620373609610",
    "finalPullDatePersian": ""
  }
  ```
  
  - get the document and you see the the `@timestamp`
  - if you want make this pipeline the default one for the `tweets`index , add it to the seeting of `tweets` :
  
  ```json
  PUT tweets
  {
    "settings": {
      "index": {
        "default_pipeline": "add-current-time"
          ...
      }
    }
  }
  ```
  
  - if you need to update `index` setting you must `reindex` it.



#### Sample Scripts

a sample code to ingest data into `tweets` index 

- install `elasticsearch` and `requests`module in python

```python
import time
import requests, json
from elasticsearch import Elasticsearch

es = Elasticsearch()

url = 'https://www.sahamyab.com/guest/twiter/list?v=0.1'
count_needed, sleep_time = 1000, 60
count_sofar = 0

while count_sofar < count_needed:
    response = requests.request('GET', url, headers={'User-Agent': 'Chrome/61'})
    result = response.status_code
    if result == requests.codes.ok:
        tweets = response.json()['items']
        for tweet in tweets:
            try:
                res = es.index(index="tweets", id=tweet["id"], body=tweet)
                print(f"tweet ID : {tweet['id']}")
                count_sofar += 1 
            except Exception as e:
                print("print exception: " + str(e))
            
    else:
        print("Response code error: " + str(result))
    print(f'Count of fetched tweets is {count_sofar}')
    time.sleep(sleep_time)


```

- test to ensure that everthing is ok :

```json
GET tweets/_search
{
  "query": {
    "exists": {
      "field": "hashtags"
    }
  }
}
```



#### Language Analyzers

![Analyzer Pipeline](https://api.contentstack.io/v2/assets/575e4c8c3dc542cb38c08267/download?uid=blt51e787daed39eae9?uid=blt51e787daed39eae9)

**[Standard Analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-standard-analyzer.html)**

The `standard` analyzer divides text into terms on word boundaries, as defined by the Unicode Text Segmentation algorithm. It removes most punctuation, lowercases terms, and supports removing stop words.

**[Simple Analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-simple-analyzer.html)**

The `simple` analyzer divides text into terms whenever it encounters a character which is not a letter. It lowercases all terms.

**[Whitespace Analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-whitespace-analyzer.html)**

The `whitespace` analyzer divides text into terms whenever it encounters any whitespace character. It does not lowercase terms.

**[Stop Analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-stop-analyzer.html)**

The `stop` analyzer is like the `simple` analyzer, but also supports removal of stop words.

**[Language Analyzers](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html)**

Elasticsearch provides many language-specific analyzers like `english` or `french`

**.....**

##### A Sample Analysis Pipeline

![A Custom Analyzer](https://api.contentstack.io/v2/assets/575e4c8c9985d58976376a3c/download?uid=bltee4e0b427d8fdad4?uid=bltee4e0b427d8fdad4)

```json
GET _analyze
{
  "text": "To learn about how usage data helps us manage and improve our products and services, see our Privacy Statement",
  "analyzer": "standard"
}


GET _analyze
{
  "text": "To learn about how usage data helps us manage and improve our products and services, see our Privacy Statement",
  "analyzer": "snowball"
}

```

#####  [Specify the analyzer for a field](https://github.com/elastic/elasticsearch/edit/7.12/docs/reference/analysis/specify-analyzer.asciidoc)

```json
PUT my-index-000001
{
  "mappings": {
    "properties": {
      "title": {
        "type": "text",
        "analyzer": "whitespace"
      }
    }
  }
}
```

#####  [Specify the analyzer for an Index](https://github.com/elastic/elasticsearch/edit/7.12/docs/reference/analysis/specify-analyzer.asciidoc)

```json
PUT my-index-000001
{
  "settings": {
    "analysis": {
      "analyzer": {
        "default": {
          "type": "simple"
        }
      }
    }
  }
}
```

[Specify the analyzer for a query](https://github.com/elastic/elasticsearch/edit/7.12/docs/reference/analysis/specify-analyzer.asciidoc)

```console
GET my-index-000001/_search
{
  "query": {
    "match": {
      "message": {
        "query": "Quick foxes",
        "analyzer": "stop"
      }
    }
  }
}
```



##### [Persian Built-in Analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/7.12/analysis-lang-analyzer.html#persian-analyzer) 

- check the  standard analyser for persian texts 

```json
GET _analyze
{
  "text": "تا می توانیم باید از آلوده کردن محیط زیست اجتناب کنیم و زباله‌هایمان را با ۱۰ ها روش نوین بازیابی کنیم"
}
```

##### define custom built-in Persian Analyzer

```json
PUT /tweets_fa
{
  "settings": {
    "index.mapping.ignore_malformed": true,
    "analysis": {
      "char_filter": {
        "zero_width_spaces": {
          "type": "mapping",
          "mappings": [
            """\u200C=>\u0020""",
              "٠ => 0",
            "١ => 1",
            "٢ => 2",
            "٣ => 3",
            "٤ => 4",
            "٥ => 5",
            "٦ => 6",
            "٧ => 7",
            "٨ => 8",
            "٩ => 9"
          ]
        }
      },
      "filter": {
        "persian_stop": {
          "type": "stop",
          "stopwords": "_persian_"
        }
      },
      "analyzer": {
        "rebuilt_persian": {
          "tokenizer": "standard",
          "char_filter": [
            "zero_width_spaces"
          ],
          "filter": [
            "lowercase",
            "decimal_digit",
            "arabic_normalization",
            "persian_normalization",
            "persian_stop"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "content": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "finalPullDatePersian": {
        "type": "date"
      },
      "id": {
        "type": "keyword"
      },
      "scoredPostDate": {
        "type": "date",
        "format": "epoch_millis||epoch_second"
      },
      "sendTime": {
        "type": "date",
        "format": "date_time_no_millis||strict_date_time_no_millis"
      },
      "sendTimePersian": {
        "type": "keyword"
      },
      "senderName": {
        "type": "keyword"
      },
      "senderProfileImage": {
        "type": "keyword"
      },
      "senderUsername": {
        "type": "keyword"
      },
      "type": {
        "type": "keyword"
      },
      "parentSendTimePersian": {
        "type": "keyword"
      },
      "parentId": {
        "type": "keyword"
      },
      "parentSenderName": {
        "type": "keyword"
      },
      "parentSenderUsername": {
        "type": "keyword"
      },
      "parentSenderProfileImage": {
        "type": "keyword"
      },
      "parentContent": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
```

- Check out above config :

```json

GET tweets_fa/_analyze
{
  "text": "تا می توانیم باید از آلوده کردن محیط زیست اجتناب کنیم و زباله‌هایمان را با ۱۰ ها روش نوین بازیابی کنیم",
   "analyzer": "rebuilt_persian"
}
```

##### Custom Persian Anlyzer

- [Nariman ParsiAnalyzer](https://github.com/NarimanN2/ParsiAnalyzer)
- inside `elasticsearch` container , run the following command :

```bash
/usr/share/elasticsearch/bin/elasticsearch-plugin install -v file:///plugins/ParsiAnalyzer-1.0-ES-7.12.0.zip
```

- restart the es
- check it again : 

```json
GET _analyze
{
  "text": "تا می توانیم باید از آلوده کردن محیط زیست اجتناب کنیم و زباله‌هایمان را با ۱۰ ها روش نوین بازیابی کنیم",
   "analyzer": "parsi"
}
```




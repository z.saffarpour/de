##### Install Logstash

- Method #1 : Direct Install ![logstash install](https://i.postimg.cc/1zKcQM11/image.png)

- Method #2 : Docker

  ```bash
  $  docker-compose -f .\docker-compose-logstash.yml up -d
  ```



##### Start the first pipeline manually

- run logstash directly from a docker container

```bash
 docker run -it --rm docker.elastic.co/logstash/logstash:7.12.0 -e 'input { stdin { } } output { stdout { codec => rubydebug } }'
```

#### Start The Cluster

```bash
$ docker-compose -f .\docker-compose-logstash.yml up
```

**we want to reading in some `csv` file**

`logstash-csv.conf` in data directory (docker-elk/logstash)

```json
input {

	file {
        path => "/data/csv/*.csv"
        start_position =>"beginning"
	      sincedb_path => "/data/pipe-csv.db"
 	}

}

filter {
  
  csv {
      separator => ","
      skip_header => "true"
      columns => ["a","b","c","d","f"]
      }
  mutate { 
    convert => ["b", "integer"] 
    rename => { "a" => "load_avg" }
    } 

 }

output {

  stdout { codec => rubydebug }
}
```

a sample csv file :

```bash
a,b,c,d,f
1,10,20,30,40
2,12,22,32,42
```



- be sure that logstash docker cluster is up & running

```bash
$ docker exec -it logstash bash
$ cd /data
$ logstash -f logstash-csv.conf --path.data /tmp
```

`--path.data PATH`  : should point to a writable directory. Logstash will use this directory whenever it needs to store data. Plugins will also have access to this path. The default is the `data` directory under Logstash home

- add some data to csv file 
- copy csv file and paste it into the same folder as `test.csv`
- stop & start logstash

```bash
$ docker-compose -f .\docker-compose-logstash.yml stop logstash
$ docker-compose -f .\docker-compose-logstash.yml start logstash
$ docker exec -it logstash bash
$ cd /data
$ logstash -f logstash-csv.conf --path.data /tmp
```

- no data output is shown in `logstash`

- delete `pipe-csv.db` &  run the above procedure again.
- every line must be output to the `stdout`

##### Send csv data to ES

- run the second logstash csv config file :

```bash
logstash -f logstash-csv-es.conf --path.data /tmp
```

- create new `spaces` in kibana
- create new index pattern
- discover the data just added ...



##### Run A Sample Pipeline

- open another console
- go to logstash container with root access :

```bash
$ docker exec -it -u 0 logstash bash
```

- install python3

```bash
$ yum update
$ yum install -y python3
$ pip3 install requests
$ chown logstash:logstash -R /data
```

- logout & enter container normally 

```bash
$ docker exec -it logstash bash
```

- run the code :

```bash
$ cd /data

$ python3 save_tweets.py

```

- run this pipeline in previous console :

```bash
$ logstash -f /data/logstash-tweets.conf --path.data /tmp
```

- add `Elasticsearch` to it & run this pipeline :

```bash
$ logstash -f /data/logstash-tweets-es.conf --path.data /tmp
```

- tweet files are removed after they are read.
- check out new tweets using `discover ` in kibana
- put this file in pipeline folder & restart  the logstash container



###### you can use [filebeat](https://logz.io/blog/filebeat-tutorial/#configurefilebeat) to ship these tweets directly into ES . 




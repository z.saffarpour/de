#### Step 1 : Single Node Cluster

- Increase Memory if needed as stated here :

  https://itnext.io/wsl2-tips-limit-cpu-memory-when-using-docker-c022535faf6f

  I used this config :

  ```toml
  [wsl2]
  memory=10GB   # Limits VM memory in WSL 2 up to 10GB
  processors=4 # Makes the WSL 2 VM use two virtual processors
  ```

  

- set `vm.max_map_count`  for running es in docker mode : 

  in Linux :  

  ```bash
  $ sudo sysctl -w vm.max_map_count=262144
  ```

  

- for windows users, using **wsl** subsystem open `Powershell` and run : 

  ```bash
  $ wsl -d docker-desktop
  ```
  
  then

  ```bash
  $ sysctl -w vm.max_map_count=262144
  $ exit
  ```



- Make sure `kibana-cluster.yml`  contains one `elasticsearch.hosts` 

  ```yaml
  elasticsearch.hosts: [ "http://es01:9200" ]
  ```

- run one elastic instance and the Kibana :

  ```bash 
  $ docker-compose -f .\docker-compose-cluster.yml up es01 kibana
  ```



#### Step 2 : Adding Second Node

- check out the `es02` configuration in `docker-compose-cluster.yml` 

- in separate console, run

```bash
$ docker-compose -f .\docker-compose-cluster.yml up es02
```

- stop the `es02` container 

#### Step3 : A Real Sample

- make sure only `es01` node is up & running


- create `tweets_fa` with following settings :

  ```json
PUT /tweets_fa
{
  "settings": {
    "number_of_replicas": 1,
    "number_of_shards": 6
  }
}
  ```

- run `ingest_tweets.py` to get some tweets

- check out the cluster/index  status 
  - `Stack Monitoring\nodes` 
  - `Stack Monitoring\indices`
    - click on index name
    - at the bottom , check out the shard distributions. 
  
- set the  `"index.number_of_replicas": "0"` in `Stack Management / Index Mnagement`
  - click on index name and edit setting in opened tab.
  - check out the cluster/index  status 
  
- set the  `"index.number_of_replicas": "1"` in `Stack Management / Index Mnagement`

- start `es02` and `es03`  node :

  ![](https://i.postimg.cc/3wTZczsj/image.png)

- **check the available Memory** if Kibana not respond:

  ```bash
  $ wsl -d docker-desktop
  $ free -m
  ```

  

- remove node `es03`

  - status becomes `Red`

  - cluster has some unassigned `replica`

    

![](https://i.postimg.cc/kM6TYrGZ/image.png)

- ​	after some while,  status turns back `Green`

![](https://i.postimg.cc/cHMw2mqv/image.png)

- add node `es04` : 

  - add new service to docker-compose file .
  - add new volume `data04`
  - ` docker-compose -f .\docker-compose-cluster.yml up es04 `

  ![](https://i.postimg.cc/xT3WfDQC/image.png)

- remove `es04`

- check the cluster status  via `Rest API`
  - got to Kibana, dev tools :

  ```bash
  $ GET /_cluster/health/tweets_fa
  
  $ GET /_cluster/stats
  
  $ GET _cat/indices/tweets_fa?v
  
  $ GET _cat/shards/tweets_fa?v
  
  $ GET _cat/nodes?v
  
  $ GET _cat/segments/tweets_fa?v
  
  $ GET _cat/allocation/?v
  
  $ GET _cat/master/?v
  ```
  
  






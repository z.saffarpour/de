import time, re
import datetime, sys
import requests, json
from elasticsearch import Elasticsearch, RequestError
import elasticapm

es = Elasticsearch(http_auth=('elastic', 'changeme'))


clientAPM = elasticapm.Client(
            # server_url=apm_url,
            # instrument=False,
            service_name='Tweets',
            # service_node_name="localhost",
            service_version="1.0",
            # hostname="localhost"
            )

regex = r"#([^\s#]+)"
tweet_ids = set()


def main() :
    clientAPM.capture_message("Getting Tweets Started: " + str(datetime.datetime.now()))
    count_sofar = 0
    count_needed, sleep_time = 1000, 60    
    url = 'https://www.sahamyab.com/guest/twiter/list?v=0.1'
    while count_sofar < count_needed:
        try : 
            response = requests.request('GET', url, headers={'User-Agent': 'Chrome/61'})
            result = response.status_code
            if result == requests.codes.ok:
                clientAPM.begin_transaction(transaction_type="script")
                tweets = response.json()['items']
                print(f"{len(tweets)} Fetched at {datetime.datetime.now()}")
                for tweet in tweets:
                    try:
                        if tweet["id"] not in tweet_ids :
                            print("- "*30)
                            print(tweet["id"])
                            print(tweet["content"])
                            # extracting the hashtags
                            tweet['hashtags'] = get_hashtags(tweet)
                            print(tweet['hashtags'])
                            res = insert_es(index="tweets_fa", id=tweet["id"], body=tweet)
                            if res :
                                count_sofar += 1
                                tweet_ids.add(tweet["id"])
                        
                    except Exception as e:
                        print("print exception: " + str(e))
                        clientAPM.capture_exception()
                clientAPM.end_transaction(name="Get & Save Sahamyab Tweets", result="success")


                    
            else:
                print("Response code error: " + str(result))
                clientAPM.capture_message("Response code error: " + str(result))
            print(f'Count of fetched tweets is {count_sofar}')
        except Exception as e:
            print("print exception: " + str(e))
            clientAPM.capture_exception()

        time.sleep(sleep_time)

@elasticapm.capture_span("Insert To ES")
def insert_es(index,id,body) :
    try:
        # res = es.index(index, id, body)
        res = es.index(index=index, id=id, body=body)
        return True
    except Exception as e:
        print("es exception: " + str(e))
        clientAPM.capture_exception()
        return False

@elasticapm.capture_span("Get Hashtags")
def get_hashtags(tweet) :
    try :
        res = re.findall(regex, tweet["content"])
        return res
    except Exception as e:
        print("Get hashtags exception: " + str(e) )  
        clientAPM.capture_exception()

        return []

if __name__ == '__main__':
    elasticapm.instrument()  # Only call this once, as early as possible.
    main()

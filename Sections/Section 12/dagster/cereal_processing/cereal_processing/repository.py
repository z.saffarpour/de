from dagster import repository

from cereal_processing.pipelines.my_pipeline import my_pipeline
from cereal_processing.pipelines.step_4 import step4_pipeline
from cereal_processing.pipelines.step_5 import step5_pipeline 
from cereal_processing.pipelines.step_6 import step6_pipeline

from cereal_processing.schedules.my_hourly_schedule import my_hourly_schedule
from cereal_processing.sensors.my_sensor import my_sensor
from cereal_processing.pipelines.cereals_sort_by_colories import cereals_sort_by_colories
from cereal_processing.schedules.step_6 import my_2minitues_cereal_check, daily_cereals
from cereal_processing.sensors.step_8 import new_cereals
@repository
def cereal_processing():
    """
    The repository definition for this cereal_processing Dagster repository.

    For hints on building your Dagster repository, see our documentation overview on Repositories:
    https://docs.dagster.io/overview/repositories-workspaces/repositories
    """
    pipelines = [step6_pipeline,my_pipeline, cereals_sort_by_colories, step4_pipeline, step5_pipeline]
    schedules = [daily_cereals, my_2minitues_cereal_check]
    sensors = [my_sensor, new_cereals]

    return pipelines + schedules + sensors

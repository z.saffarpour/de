from dagster import ModeDefinition, pipeline, PresetDefinition
from cereal_processing.solids.step_6 import read_csv_step_6, sort_by_calories_step6

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})

@pipeline(mode_defs=[MODE_DEV, MODE_TEST] , preset_defs=[
    PresetDefinition(
                name="Sample_Cereals_Test",
                run_config={"solids": {"read_csv_step_6": {"config": {"csv_name": "cereal.csv"} } } },
                mode='test'
            )
    ]
)
def step6_pipeline():
    sort_by_calories_step6(read_csv_step_6())


### First Try

```bash
$ pip install target-csv tap-exchangeratesapi
```

Error !

#### Install Each Tap/Target in dedicated VENV

- **exchangeratesapi**

```bash
$ python -m venv .venv-exchangeratesapi
$ .\.venv-exchangeratesapi\Scripts\activate
$ pip install tap-exchangeratesapi
$ deactivate
$ .\.venv-exchange\Scripts\tap-exchangeratehost.exe
```

Error! (It Needs Access key)

see details : [The tap fails with KeyError: 'rates' · Issue #13 · singer-io/tap-exchangeratesapi (github.com)](https://github.com/singer-io/tap-exchangeratesapi/issues/13)

- [**tap-exchangeratehost**](https://github.com/anelendata/tap-exchangeratehost)

https://github.com/anelendata/tap-exchangeratehost

```bash
$ python -m venv .venv-exchange
$ .venv-exchange/Scripts/activate
$ pip install tap-exchangeratesapi
$ deactivate
$ .\.venv-exchange\Scripts\tap-exchangeratehost.exe

```

-  **target-csv**

```bash
$ python -m venv .venv-csv
$ .venv-csv/Scripts/activate
$ pip install target-csv
$ deactivate
$ .\.venv-exchange\Scripts\tap-exchangeratehost.exe | .\.venv-csv\Scripts\target-csv.exe
or 
$ .\.venv-exchange\Scripts\tap-exchangeratehost.exe | .\.venv-csv\Scripts\target-csv.exe  --config .\csv-config.json

```



#### Reading Data From MongoDB

- setup mongo cluster (`docker-compose -f .\docker-compose-mongo.yml up`)

- got to `http://localhost:8081`

- create  database `tweetes`  

- within `tweets` , create collection `sahamyab`

- insert some tweets from sahamyab in json format

   

```bash
$ python -m venv .venv-mongodb
$ .venv-mongodb/Scripts/activate
$ pip install tap-mongodb
$ deactivate
$ tap-mongodb --config ./config.json --discover > ./catalog.json
$ edit `catalog.json` and for table `sahamyab` add these lines to internal metadata of desired stream :
"selected": true,
"replication-method": "FULL_TABLE"
 
$ .\.venv-mongodb\Scripts\tap-mongodb.exe --config .\config.json --catalog .\catalog.json 
 
$ .\.venv-mongodb\Scripts\tap-mongodb.exe --config .\config.json --catalog .\catalog.json | .\.venv-csv\Scripts\target-csv.exe --config .\csv-config.json
```



Error Charmap !

#### Customization of Target-CSV

---

ِYou May Need To Install `Visual Studi Build Tools` - download VS Installer & Select Only Build Tools.

---

- fork the main repo : `https://github.com/singer-io/target-csv`
- clone it , for example : `git clone https://github.com/dbrg-ut/target-csv target-csv-dev`
- Add `utf-8` support to `target_csv.py`
- or replace it with the file provided in singer folder.
- make another `VENV` - `.venv-target-csv`
- activate it & **change directory into  `target-csv-dev` folder**
- run ` python -m pip install --upgrade pip`
- run : `python setup.py install`
- get data from Mongo :

```bash
$ .\.venv-mongodb\Scripts\tap-mongodb.exe --config .\config.json --catalog .\catalog.json | .\.venv-target-csv\Scripts\target-csv.exe  --config .\csv-config.json
```



#### Developing A Sahamyab Tap

- create new Virtual Env : `.venv-tap-sahamyab` and Activate It

- `pip install cookiecutter`

- `cookiecutter https://github.com/singer-io/singer-tap-template.git`

- cd `tap-sahamyab`

- `pip install -e .`

- `tap-sahamyab.exe -h`

- `pip install singer-python`

- code .

- edit `__init__.py` in `tap-sahamyab\tap-sahamyab`  (copy it from `codes` folder)

- run in bash :`tap-sahamyab`

- deactivate

- cd ..

- ` .\.venv-tap-sahamyab\Scripts\tap-sahamyab.exe | .\.venv-target-csv\Scripts\target-csv.exe --config .\csv-config.json`

  Sample Output : 

  ```bash
  INFO Sending version information to singer.io. To disable sending anonymous usage data, set the config parameter "disable_collection" to true
  INFO tweet with id : 276123945 from ario1397 was sent to stdout
  INFO tweet with id : 276123941 from mahdighk was sent to stdout
  INFO tweet with id : 276123929 from ab10ab10ab10ab1 was sent to stdout
  INFO tweet with id : 276123925 from ravandsooadi was sent to stdout
  INFO tweet with id : 275498274 from 3_white_soldier was sent to stdout
  INFO tweet with id : 275500250 from 3_white_soldier was sent to stdout
  INFO tweet with id : 275638007 from 3_white_soldier was sent to stdout
  INFO tweet with id : 276123768 from 121212ebiasl was sent to stdout
  INFO tweet with id : 276123765 from mohammad778697 was sent to stdout
  INFO tweet with id : 275643342 from 3_white_soldier was sent to stdout
  ```

  

  

